
Modify `cache/group_ids.csv` with the ids of the groups you want to duplicate on on Mastodon as Qoto groups.

Store the API_keys of the app(s) declared for the Qoto account in files placed in this root directory of the repo and named:
`qoto_<GROUP_ID>_key`

Build and run the container.

```
docker build . -t fb2toot-qotogroups
docker run -v $(pwd)/cache:/cache fb2toot-qotogroups
```

You may add or remove groups without rebuilding the container.
