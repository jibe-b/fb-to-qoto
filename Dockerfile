FROM ubuntu:18.04

RUN apt update -qq && apt install -y wget build-essential python3 python3-dev libxml2-dev libxslt-dev python-dev python3-pip

COPY cache/ /
COPY fb2toot-qotogroups.py /
COPY requirements.txt /
COPY qoto_*_apikey /
RUN pip3 --cache-dir cache/.pip_cache install -r requirements.txt

WORKDIR .
CMD ["python3", "fb2toot-qotogroups.py"]
