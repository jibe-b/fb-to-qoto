from atoma import parse_atom_bytes
from requests import get, post
from bs4 import BeautifulSoup
from html2text import html2text
from re import sub, search, match
from subprocess import run
from mastodon import Mastodon
from glob import glob


fb_group_ids = list(set(open("cache/group_ids.csv", 'r').read().split("\n")))
if "" in fb_group_ids:
    fb_group_ids.remove("")

base_feed_url = "https://feed.eugenemolotov.ru/?action=display&bridge=Facebook&context=Group&limit=-1&format=Atom&g=https%3A%2F%2Fwww.facebook.com%2Fgroups%2F"

for fb_group_id in fb_group_ids:
    print(fb_group_id)
    feed_url = base_feed_url + fb_group_id

    response = get(feed_url)
    feed = parse_atom_bytes(response.content)

    cache_already_posted = "cache/already_posted-{}.csv".format(fb_group_id)

    already_posted = list(set(open(cache_already_posted, 'r').read().split("\n")))

    api_key = open("qoto_{}_apikey".format(fb_group_id)).read()[:-1]
    
    mastodon = Mastodon(
        access_token = api_key,
        api_base_url = 'https://groups.qoto.org'
    )
    
    for item in feed.entries:
        id_ = item.id_
        if id_ not in already_posted and not search("urn:sha1:" ,id_) and search("facebook.com/groups/{}/".format(fb_group_id), id_):
            print(id_)
            
            pictures_dir = "/tmp/pictures-{}".format(fb_group_id)
            run(["rm", "-rf", pictures_dir])
            for picture in [link.href for link in item.links if match("https://scontent", link.href)]:
                run(["wget", "-P", pictures_dir, picture])

            media_ids = []
            for picture_file in glob("{}/*".format(pictures_dir)):
                response = mastodon.media_post(picture_file, "image/jpeg")
                media_ids.append(str(response.id))
                                 
            html = BeautifulSoup(item.content.value, features="lxml")
            a = "".join([html2text(str(p)) for p in html.body.findAll("p")])
            b = sub("\!\[\]\(https://static.xx.fbcdn.net/images/emoji.php/v9/.../1/16/1f...\.png\)", "", a)
            c = sub(r"…\n\[Meer\]([^)]*)\)\n", " ", b)
            d = sub("\n", "NEWLINE", c)
            e = sub(r"\[([^\]]*)\]\([^)]*\)", r"\1", d)
            f = sub("NEWLINE", "\n", e)
            g = "@" + item.authors[0].name + " «" + f
            h = g[:400] + "[…] »"
            status = h + "\nLire plus: " + sub("m\.", "", item.id_)

            mastodon.status_post(status, media_ids=media_ids[:4]) #Max 4 attachments
            already_posted.append(id_)
            open(cache_already_posted, 'a').write(id_ + "\n")
